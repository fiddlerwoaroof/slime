;;; swank-output-streams.lisp --- Streams that output to a buffer
;;;
;;; Authors: Ed Langley  <el-github@elangley.org>
;;;
;;; License: This code has been placed in the Public Domain.  All warranties
;;;          are disclaimed.

(in-package :swank)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (swank-require :swank-repl))

(defun make-buffer-output-stream (target-identifier)
  (send-to-emacs `(:make-target ,(current-thread-id)
                                ,target-identifier))
  (wait-for-event `(:target-created ,(current-thread-id) ,target-identifier))
  (swank-repl::make-output-stream-for-target *emacs-connection* target-identifier))

(provide :swank-output-streams)
